package rename_no_KR;

import java.io.File;

public class Main {
	public static void main(String[] args) throws Exception {
		String currPath = System.getProperty("user.dir") + '\\';
		char[] currFileName;
		String[] reservedFileNames = {"CON", "PRN", "AUX", "CLOCK$", "NUL", "COM1", "COM2", "COM3", "COM4", "LPT1", 
				"LPT2", "LPT3", "LPT4", "LST", "KEYBD$", "SCREEN$", "$IDLE$", "CONFIG$"};
		File folder = new File(currPath);
		File[] listOfFiles = folder.listFiles();
		boolean changeable = false;
		String errorMsg = "";
		
	    for (File f: listOfFiles) {
			StringBuilder temp = new StringBuilder();

	    	currFileName = (f.getName()).toCharArray();	    	
	    	
	    	System.out.println(currPath + f.getName());
	    	
	    	if (currFileName[0] == '.') 
	    		continue;
	    		    	
	    	for (char c: currFileName) {
	    		if (isNotKr(c)) 
	    			temp.append(c);
	    		if (c == '.') {
	    			changeable = true;
	    		}
	    	}
	    	
	    	for (String reserved: reservedFileNames) {
	    		if (temp.equals(reserved)) {
	    			errorMsg = "it is reserved file name";
			    	System.out.println("Could not change file: \"" + f.getName() + "\" for " + errorMsg);
	    			continue;
	    		}
	    	}
	    	 	
	    	File newFile = new File(temp.toString());
	    	if (newFile.exists()) {
	    		errorMsg = "the file it already exists.";
		    	System.out.println("Could not change file: \"" + f.getName() + "\" for " + errorMsg);
	    		continue;
	    	}
	    	
	    	if (temp.equals("")) {
	    		errorMsg = "file name is null.";
		    	System.out.println("Could not change file: \"" + f.getName() + "\" for " + errorMsg);
	    		continue;
	    	}

	    	if (changeable) {
	    		f.renameTo(newFile);
		    	System.out.println("Changing file: " + f.getName() + "   to   " + temp.toString() +".");
	    	} 
	    	
	    	changeable = false;
	    }
	}
	
	public static boolean isNotKr(char c) {
		System.out.println("char: '" + c + "' ... " + (int) c);
		
		if ((int) c < 0 || c > 127) {
			System.out.println("false not ascii");

			return false;
		}
		
		String specialChars = "<>:\"/\\|?*";
			
		if (specialChars.contains(String.valueOf(c))) { 
			System.out.println("false sym");

			return false; 
		}
//		
//		if (!Character.isLetterOrDigit(c)) { 
//			System.out.println("false l or d");
//
//			return false;
//			
//		}		
				
		return true;
	}
}
